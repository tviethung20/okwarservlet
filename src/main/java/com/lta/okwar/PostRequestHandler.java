package com.lta.okwar;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public abstract class PostRequestHandler implements IRequestHandler {
    public static String AUTHORIZATION = "authorization";
//    public String body;
    public ObjectNode sfsObject = new ObjectNode(JsonNodeFactory.instance);
    public ISFSExtension myExtension;

    @Override
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp, boolean isPublic) throws IOException {
        try {
            this.myExtension = myExtension;
            List<String> headNames = Collections.list(req.getHeaderNames());
            checkHeader(headNames, req, AUTHORIZATION);
            // Read from request
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = req.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
                buffer.append(System.lineSeparator());
            }
            String body = buffer.toString();
//            resp.getWriter().write(Utils.getJsonFromEncrypt(body.trim()));
            if(!isPublic) {
                sfsObject.put("body", Utils.getJsonFromEncrypt(body.trim()));
            } else {
                sfsObject.put("body", body);
            }
            resp.getWriter().write(SendRequest().toString());
        } catch (Exception e) {
            resp.sendError(400, "Exception: " + e.getMessage());
        }
    }

    public void checkHeader(List<String> headers, HttpServletRequest req, String key) {
        if(headers.contains(key)) {
            String value = req.getHeader(key).trim();
            sfsObject.put(key, value);
        }
    }

    public abstract Object SendRequest();
}
