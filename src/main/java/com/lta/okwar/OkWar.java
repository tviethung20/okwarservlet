package com.lta.okwar;

import com.lta.okwar.config.*;
import com.lta.okwar.hero.LockHeroHandler;
import com.lta.okwar.hero.UnLockHeroHandler;
import com.lta.okwar.market.*;
import com.lta.okwar.report.ArenaLogHandler;
import com.lta.okwar.report.ShopLogHandler;
import com.lta.okwar.report.StaminaLogHandler;
import com.lta.okwar.report.UserLogHandler;
import com.lta.okwar.transition.*;
import com.lta.okwar.transition.equipment.EquipmentAuctionSuccessHandler;
import com.lta.okwar.transition.equipment.EquipmentCancelSellHandler;
import com.lta.okwar.transition.equipment.EquipmentSellingHandler;
import com.lta.okwar.transition.equipment.EquipmentTransferHandler;
import com.lta.okwar.user.UserBalanceHandler;
import com.lta.okwar.user.UserHeroesHandler;
import com.lta.okwar.user.UserInventoryEquipmentHandler;
import com.lta.okwar.user.UserInventoryHandler;
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(name = "okwar", urlPatterns = {
        Utils.BUY_CHEST_API,
        Utils.TRANSFER_API,
        Utils.SELLING_API,
        Utils.CANCEL_SELL_API,
        Utils.AUCTION_API,
        Utils.LOGIN_API,
        Utils.TRANSACTION_WITHDRAW_POINT_API,
        Utils.HERO_CHECK_FUSION_API,
        Utils.UPGRADE_HERO_API,
        Utils.MARKET_API,
        Utils.MARKET_NFT_INFORMATION_API,
        Utils.BALANCE_API,
        Utils.LOCK_HERO_API,
        Utils.UNLOCK_HERO_API,
        Utils.OPEN_CHEST_API,
        Utils.INVENTORY_NFT_INFORMATION_API,
        Utils.TRANSACTION_DEPOSIT_POINT_API,
        Utils.LIST_NFT_INFORMATION_API,
        Utils.HERO_IMAGE_API,
        Utils.HERO_REPORT_API,
        Utils.HERO_LIST_API,
        Utils.LINK_EQUIPMENT_API,
        Utils.BUY_STAMINA_API,
        Utils.LIST_EQUIPMENT_NFT_INFORMATION_API,
        Utils.MARKET_EQUIPMENT_API,
        Utils.EQUIPMENT_LIST_API,
        Utils.GET_CONFIG_API,
        Utils.TRANSFER_EQUIPMENT_API,
        Utils.SELLING_EQUIPMENT_API,
        Utils.CANCEL_EQUIPMENT_SELL_API,
        Utils.EQUIPMENT_AUCTION_API,
        Utils.EQUIPMENT_IMAGE_API,
        Utils.EQUIPMENT_NFT_INFORMATION_API,
        Utils.CHANGE_SHOP_CONFIG_API,
        Utils.CHANGE_STAMINA_CONFIG_API,
        Utils.CHANGE_PVP_CONFIG_API,
        Utils.REFUND_ENO_API,
        Utils.REFUND_STAMINA_API,
        Utils.REFUND_ITEM_API,
        Utils.REPORT_SHOP_API,
        Utils.REPORT_STAMINA_API,
        Utils.REPORT_ARENA_API,
        Utils.REPORT_USER_API,
        Utils.TRANSACTION_WITHDRAW_AMB_API,
        Utils.COOP_REWARD_API
})
public class OkWar extends HttpServlet {

    public static SmartFoxServer sfs;
    public static ISFSExtension myExtension;

    @Override
    public void init() throws ServletException
    {
        if(sfs == null) {
            sfs = SmartFoxServer.getInstance();
//            myExtension = sfs.getZoneManager().getZoneByName("Shibamons").getExtension();
            myExtension = sfs.getZoneManager().getZoneByName("OkWar").getExtension();
//            myExtension = sfs.getZoneManager().getZoneByName("BattleMons").getExtension();
        }
    }

    /*private static SmartFoxServer sfs;
    private static ISFSExtension myExtension;

    @Override
    public void init() throws ServletException
    {
        sfs = SmartFoxServer.getInstance();
        myExtension = sfs.getZoneManager().getZoneByName("Farm").getExtension();
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        /*resp.addHeader("Access-Control-Allow-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Request-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Allow-Methods", "*");
        resp.addHeader("Content-Type", "application/json");*/
        /*resp.addHeader("Access-Control-Allow-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Allow-Methods",
                "GET, OPTIONS, HEAD, PUT, POST, DELETE");
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Max-Age", "86400");*/
//        resp.addHeader("Access-Control-Allow-Origin", "*");
//        resp.addHeader("Access-Control-Allow-Headers", "*");
//        resp.addHeader("Content-Type", "text/html");
        /* resp.addHeader("Access-Control-Allow-Headers", "*");
        resp.addHeader("Access-Control-Allow-Methods",
            "GET, OPTIONS, HEAD, PUT, POST, DELETE");
        resp.addHeader("Access-Control-Allow-Origin", "*");*/
        postProcess(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*resp.addHeader("Access-Control-Allow-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Request-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Allow-Methods",
                "GET, OPTIONS, HEAD, PUT, POST, DELETE");
        resp.addHeader("Content-Type", "application/json");*/
//        resp.addHeader("Access-Control-Allow-Origin", "*");
//        resp.addHeader("Content-Type", "text/html");
        /*resp.addHeader("Access-Control-Allow-Headers", "*");
        resp.addHeader("Access-Control-Allow-Methods",
                "GET, OPTIONS, HEAD, PUT, POST, DELETE");
        resp.addHeader("Access-Control-Allow-Origin", "*");*/
        postProcess(req, resp);
    }

    private void postProcess(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String url = req.getServletPath();
        switch (url) {
            case Utils.BUY_CHEST_API:
                BuyChestHandler buyChestHandler = new BuyChestHandler();
                buyChestHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.OPEN_CHEST_API:
                OpenChestHandler openChestHandler = new OpenChestHandler();
                openChestHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.TRANSFER_API:
                TransferHandler transferHandler = new TransferHandler();
                transferHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.SELLING_API:
                SellingHandler sellingHandler = new SellingHandler();
                sellingHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.CANCEL_SELL_API:
                CancelSellHandler cancelSellHandler = new CancelSellHandler();
                cancelSellHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.AUCTION_API:
                AuctionSuccessHandler auctionSuccessHandler = new AuctionSuccessHandler();
                auctionSuccessHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.BALANCE_API:
                UserBalanceHandler userBalanceHandler = new UserBalanceHandler();
                userBalanceHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.HERO_CHECK_FUSION_API:
                CheckFusionHandler checkFusionHandler = new CheckFusionHandler();
                checkFusionHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.UPGRADE_HERO_API:
                UpgradeHeroHandler upgradeHeroHandler = new UpgradeHeroHandler();
                upgradeHeroHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.MARKET_API:
                MarketHandler marketHandler = new MarketHandler();
                marketHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.MARKET_NFT_INFORMATION_API:
                TokenInformationHandler tokenInformationHandler = new TokenInformationHandler();
                tokenInformationHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.LOCK_HERO_API:
                LockHeroHandler lockHeroHandler = new LockHeroHandler();
                lockHeroHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.UNLOCK_HERO_API:
                UnLockHeroHandler unLockHeroHandler = new UnLockHeroHandler();
                unLockHeroHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.INVENTORY_NFT_INFORMATION_API:
                UserInventoryHandler userInventoryHandler = new UserInventoryHandler();
                userInventoryHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.LIST_NFT_INFORMATION_API:
                ListTokenInformationHandler listTokenInformationHandler = new ListTokenInformationHandler();
                listTokenInformationHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.TRANSACTION_DEPOSIT_POINT_API:
                BuyPointHandler buyPointHandler = new BuyPointHandler();
                buyPointHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.TRANSACTION_WITHDRAW_POINT_API:
                ReducePointHandler reducePointHandler = new ReducePointHandler();
                reducePointHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.HERO_IMAGE_API:
                HeroAvatarHandler heroAvatarHandler = new HeroAvatarHandler();
                heroAvatarHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.HERO_REPORT_API:
                ReportHeroHandler reportHeroHandler = new ReportHeroHandler();
                reportHeroHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.HERO_LIST_API:
                UserHeroesHandler userHeroesHandler = new UserHeroesHandler();
                userHeroesHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.LINK_EQUIPMENT_API:
                LinkEquipmentHandler linkEquipmentHandler = new LinkEquipmentHandler();
                linkEquipmentHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.BUY_STAMINA_API:
                BuyStaminaHandler buyStaminaHandler = new BuyStaminaHandler();
                buyStaminaHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.LIST_EQUIPMENT_NFT_INFORMATION_API:
                ListEquipmentTokenInformationHandler equipmentTokenInformationHandler = new ListEquipmentTokenInformationHandler();
                equipmentTokenInformationHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.MARKET_EQUIPMENT_API:
                MarketEquipmentHandler marketEquipmentHandler = new MarketEquipmentHandler();
                marketEquipmentHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.EQUIPMENT_LIST_API:
                UserInventoryEquipmentHandler inventoryEquipmentHandler = new UserInventoryEquipmentHandler();
                inventoryEquipmentHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.GET_CONFIG_API:
                ConfigHandler getConfigHandler = new ConfigHandler();
                getConfigHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.TRANSFER_EQUIPMENT_API:
                EquipmentTransferHandler transferHandler1 = new EquipmentTransferHandler();
                transferHandler1.handlerRequest(myExtension, req, resp);
                break;
            case Utils.SELLING_EQUIPMENT_API:
                EquipmentSellingHandler equipmentSellingHandler = new EquipmentSellingHandler();
                equipmentSellingHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.CANCEL_EQUIPMENT_SELL_API:
                EquipmentCancelSellHandler equipmentCancelSellHandler = new EquipmentCancelSellHandler();
                equipmentCancelSellHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.EQUIPMENT_AUCTION_API:
                EquipmentAuctionSuccessHandler equipmentAuctionSuccessHandler = new EquipmentAuctionSuccessHandler();
                equipmentAuctionSuccessHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.EQUIPMENT_IMAGE_API:
                EquipmentAvatarHandler equipmentAvatarHandler = new EquipmentAvatarHandler();
                equipmentAvatarHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.EQUIPMENT_NFT_INFORMATION_API:
                EquipmentTokenInformationHandler equipmentTokenInformationHandler1 = new EquipmentTokenInformationHandler();
                equipmentTokenInformationHandler1.handlerRequest(myExtension, req, resp);
                break;
            case Utils.CHANGE_SHOP_CONFIG_API:
                ChangeShopHandler changeShopHandler = new ChangeShopHandler();
                changeShopHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.CHANGE_STAMINA_CONFIG_API:
                ChangeStaminaConfigHandler changeStaminaConfigHandler = new ChangeStaminaConfigHandler();
                changeStaminaConfigHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.CHANGE_PVP_CONFIG_API:
                ChangePvPConfigHandler changePvPConfigHandler = new ChangePvPConfigHandler();
                changePvPConfigHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REFUND_ENO_API:
                RefundEnoHandler refundEnoHandler = new RefundEnoHandler();
                refundEnoHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REFUND_STAMINA_API:
                RefundStaminaHandler refundStaminaHandler = new RefundStaminaHandler();
                refundStaminaHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REFUND_ITEM_API:
                RefundItemHandler refundItemHandler = new RefundItemHandler();
                refundItemHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REPORT_SHOP_API:
                ShopLogHandler shopLogHandler = new ShopLogHandler();
                shopLogHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REPORT_STAMINA_API:
                StaminaLogHandler staminaLogHandler = new StaminaLogHandler();
                staminaLogHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REPORT_ARENA_API:
                ArenaLogHandler arenaLogHandler = new ArenaLogHandler();
                arenaLogHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.REPORT_USER_API:
                UserLogHandler userLogHandler = new UserLogHandler();
                userLogHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.TRANSACTION_WITHDRAW_AMB_API:
                WithdrawAMBHandler withdrawAMBHandler = new WithdrawAMBHandler();
                withdrawAMBHandler.handlerRequest(myExtension, req, resp);
                break;
            case Utils.COOP_REWARD_API:
                CoopConfigHandler coopConfigHandler = new CoopConfigHandler();
                coopConfigHandler.handlerRequest(myExtension, req, resp);
                break;
        }
    }
}