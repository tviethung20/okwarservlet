package com.lta.okwar.transition;

import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.PostRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpgradeHeroHandler extends PostRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
            super.handlerRequest(myExtension, req, resp, false);
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.UPGRADE_HERO_CMD, sfsObject);
    }
}
