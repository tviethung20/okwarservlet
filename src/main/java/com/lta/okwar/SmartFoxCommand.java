package com.lta.okwar;

public class SmartFoxCommand {
    public static final String BUY_CHEST_CMD= "BuyChest";
    public static final String OPEN_CHEST_CMD= "OpenChest";
    public static final String GET_INVENTORY_CMD= "GetInventory";
    public static final String TRANSFER_TOKEN_CMD= "TransferToken";
    public static final String SELLING_TOKEN_CMD= "SellingToken";
    public static final String CANCEL_SELLING_TOKEN_CMD= "CancelSellingToken";
    public static final String AUCTION_SUCCESS_CMD= "AuctionSuccessToken";
    public static final String GET_USER_INFORMATION_CMD= "GetUserInformation";
    public static final String GET_TOKEN_INFORMATION_LIST_CMD= "GetTokenInformationList";
    public static final String GET_TOKEN_DETAIL_CMD= "GetTokenDetail";
    public static final String CHECK_FUSION_HERO_CMD= "CheckFusionHero";
    public static final String UPGRADE_HERO_CMD= "UpgradeHero";
    public static final String LOCK_HERO_CMD= "LockHero";
    public static final String UNLOCK_HERO_CMD= "UnlockHero";
    public static final String REDUCE_POINT_CMD= "ReducePoint";

    public static final String WITHDRAW_AMB_CMD= "WithdrawAMB";
    public static final String BUY_POINT_CMD= "BuyPoint";
    public static final String LIST_TOKEN_IDS_INFORMATION_CMD= "ListTokenIdsInformation";
    public static final String HERO_TOKEN_AVATAR_CMD = "HeroTokenAvatar";
    public static final String REPORT_HERO_CMD = "ReportHero";
    public static final String LIST_HERO_TOKENS_CMD = "ListHeroTokens";
    public static final String GET_EQUIPMENT_INVENTORY_CMD= "GetEquipmentInventory";
    public static final String GET_MARKET_EQUIPMENT_LIST_CMD= "GetMarketEquipmentInformationList";
    public static final String LIST_EQUIPMENT_TOKEN_IDS_INFORMATION_CMD= "ListEquipmentTokenIdsInformation";
    public static final String BUY_STAMINA_CMD= "BuyStamina";
    public static final String LINK_EQUIPMENT_CMD= "LinkEquipment";

    //Equipment
    public static final String TRANSFER_EQUIPMENT_TOKEN_CMD= "TransferEquipmentToken";
    public static final String SELLING_EQUIPMENT_TOKEN_CMD= "SellingEquipmentToken";
    public static final String CANCEL_SELLING_EQUIPMENT_TOKEN_CMD= "CancelSellingEquipmentToken";
    public static final String AUCTION_EQUIPMENT_SUCCESS_CMD= "AuctionEquipmentTokenSuccess";
    public static final String GET_EQUIPMENT_TOKEN_DETAIL_CMD= "GetEquipmentTokenDetail";
    public static final String EQUIPMENT_TOKEN_AVATAR_CMD = "EquipmentTokenAvatar";

    //Internal API
    public static final String GET_CONFIG_CMD= "GetConfig";
    public static final String CHANGE_SHOP_ITEM_CMD= "ChangeShopItem";
    public static final String CHANGE_STAMINA_CONFIG_CMD= "ChangeStaminaConfig";
    public static final String CHANGE_PVP_CONFIG_CMD= "ChangePVPConfig";
    public static final String REFUND_ENO_CONFIG_CMD= "RefundEnoConfig";
    public static final String REFUND_STAMINA_CONFIG_CMD= "RefundStaminaConfig";
    public static final String REFUND_ITEM_CMD= "RefundItemConfig";

    public static final String COOP_REWARD_CMD= "CoopRewardConfig";

    //Report API
    public static final String SHOP_LOG_CMD= "ShopLogReport";
    public static final String STAMINA_LOG_CMD= "StaminaLogReport";
    public static final String ARENA_LOG_CMD= "ArenaLogReport";
    public static final String USER_LOG_CMD= "UserLogReport";
}
