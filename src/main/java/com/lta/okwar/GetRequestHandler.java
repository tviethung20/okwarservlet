package com.lta.okwar;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public abstract class GetRequestHandler implements IRequestHandler {
    public static String AUTHORIZATION = "authorization";
    public ObjectNode sfsObject;
    public ISFSExtension myExtension;
    List<String> headNames = new ArrayList<>();
    List<String> paramNames = new ArrayList<>();

    @Override
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp, boolean isPublic) throws IOException {
        this.myExtension = myExtension;
        sfsObject = new ObjectNode(JsonNodeFactory.instance);
        for (Enumeration<String> e = req.getHeaderNames(); e.hasMoreElements();)
            headNames.add(e.nextElement());
        for (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements();)
            paramNames.add(e.nextElement());
        checkHeader(req, AUTHORIZATION);
        /*resp.addHeader("Access-Control-Request-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
        resp.addHeader("Access-Control-Allow-Methods", "*");
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Credentials", "true");*/
    }

    public void checkHeader(HttpServletRequest req, String key) {
        if(headNames.contains(key)) {
            String value = req.getHeader(key).trim();
            sfsObject.put(key, value);
        }
    }

    public void checkParams(HttpServletRequest req, String key) {
        if(paramNames.contains(key)) {
            String value = req.getParameter(key).trim();
            sfsObject.put(key, value);
        }
    }

    public void checkParamsMap(HttpServletRequest req, String key) {
        if(paramNames.contains(key)) {
            String[] value = req.getParameterMap().get(key);
            sfsObject.put(key, String.join(",", value));
        }
    }
    public abstract Object SendRequest();
}
