package com.lta.okwar.config;

import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.PostRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangeShopHandler extends PostRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
            super.handlerRequest(myExtension, req, resp, true);
//            resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.CHANGE_SHOP_ITEM_CMD, sfsObject);
    }
}
