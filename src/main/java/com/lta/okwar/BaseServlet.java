package com.lta.okwar;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class BaseServlet extends HttpServlet {

    public static SmartFoxServer sfs;
    public static ISFSExtension myExtension;

    @Override
    public void init() throws ServletException
    {
        if(sfs == null) {
            sfs = SmartFoxServer.getInstance();
            myExtension = sfs.getZoneManager().getZoneByName("Farm").getExtension();
        }
    }
}
