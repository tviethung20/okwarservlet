package com.lta.okwar.user;

import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserInventoryHandler extends GetRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        super.handlerRequest(myExtension, req, resp, false);
        checkParams(req, "page");
        checkParams(req, "page_size");
        checkParams(req, "isEgg");
        checkParams(req, "is_on_auction");
        resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.GET_INVENTORY_CMD, sfsObject);
    }
}
