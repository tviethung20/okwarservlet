package com.lta.okwar.user;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.PostRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class UserBalanceHandler extends GetRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
            super.handlerRequest(myExtension, req, resp, false);
            resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.GET_USER_INFORMATION_CMD, sfsObject);
    }
}
