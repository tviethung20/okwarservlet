package com.lta.okwar;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

public class Utils {
    public static String SECRET_KEY = "URgnrceNxLVMPlFfrinnkyWiSCEPhiPx"; //Shibamon
//    public static String SECRET_KEY = "BTMmideWnSUlaVEnyONAncentIBlasMe"; //Battlememe


    public static byte[][] GenerateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password, MessageDigest md) {

        int digestLength = md.getDigestLength();
        int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
        byte[] generatedData = new byte[requiredLength];
        int generatedLength = 0;

        try {
            md.reset();

            // Repeat process until sufficient data has been generated
            while (generatedLength < keyLength + ivLength) {

                // Digest data (last digest if available, password data, salt if available)
                if (generatedLength > 0)
                    md.update(generatedData, generatedLength - digestLength, digestLength);
                md.update(password);
                if (salt != null)
                    md.update(salt, 0, 8);
                md.digest(generatedData, generatedLength, digestLength);

                // additional rounds
                for (int i = 1; i < iterations; i++) {
                    md.update(generatedData, generatedLength, digestLength);
                    md.digest(generatedData, generatedLength, digestLength);
                }

                generatedLength += digestLength;
            }

            // Copy key and IV into separate byte arrays
            byte[][] result = new byte[2][];
            result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
            if (ivLength > 0)
                result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

            return result;

        } catch (DigestException e) {
            throw new RuntimeException(e);

        } finally {
            // Clean out temporary data
            Arrays.fill(generatedData, (byte)0);
        }
    }

    public static String getJsonFromEncrypt(String encryptedString) throws Exception{
        byte[] cipherData = Base64.getDecoder().decode(encryptedString);
        byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);

        MessageDigest md5 = MessageDigest.getInstance("MD5");
        final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, SECRET_KEY.getBytes(StandardCharsets.UTF_8), md5);
        SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
        IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

        byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
        Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
        aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
        byte[] decryptedData = aesCBC.doFinal(encrypted);
        String decryptedText = new String(decryptedData, StandardCharsets.UTF_8);
        return decryptedText;
    }

    public static String decryptMessage(String message) throws Exception {
        SecureRandom sr = new SecureRandom();
        byte[] salt = new byte[8];
        sr.nextBytes(salt);
        final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, salt, SECRET_KEY.getBytes(StandardCharsets.UTF_8),
                MessageDigest.getInstance("MD5"));
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", BouncyCastleProvider.PROVIDER_NAME);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyAndIV[0], "AES"), new IvParameterSpec(keyAndIV[1]));
        byte[] encryptedData = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
        byte[] prefixAndSaltAndEncryptedData = new byte[16 + encryptedData.length];
        // Copy prefix (0-th to 7-th bytes)
        System.arraycopy("Salted__".getBytes(StandardCharsets.UTF_8), 0, prefixAndSaltAndEncryptedData, 0, 8);
        // Copy salt (8-th to 15-th bytes)
        System.arraycopy(salt, 0, prefixAndSaltAndEncryptedData, 8, 8);
        // Copy encrypted data (16-th byte and onwards)
        System.arraycopy(encryptedData, 0, prefixAndSaltAndEncryptedData, 16, encryptedData.length);
        String encode = Base64.getEncoder().encodeToString(prefixAndSaltAndEncryptedData);
        return encode;
    }

    /*public static void main(String[] args) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            String stringToEncrypt = "Hello world 12345";
            SecureRandom sr = new SecureRandom();
            byte[] salt = new byte[8];
            sr.nextBytes(salt);
            final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, salt, SECRET_KEY.getBytes(StandardCharsets.UTF_8),
                    MessageDigest.getInstance("MD5"));
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", BouncyCastleProvider.PROVIDER_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyAndIV[0], "AES"), new IvParameterSpec(keyAndIV[1]));
            byte[] encryptedData = cipher.doFinal(stringToEncrypt.getBytes(StandardCharsets.UTF_8));
            byte[] prefixAndSaltAndEncryptedData = new byte[16 + encryptedData.length];
            // Copy prefix (0-th to 7-th bytes)
            System.arraycopy("Salted__".getBytes(StandardCharsets.UTF_8), 0, prefixAndSaltAndEncryptedData, 0, 8);
            // Copy salt (8-th to 15-th bytes)
            System.arraycopy(salt, 0, prefixAndSaltAndEncryptedData, 8, 8);
            // Copy encrypted data (16-th byte and onwards)
            System.arraycopy(encryptedData, 0, prefixAndSaltAndEncryptedData, 16, encryptedData.length);
            String encode = Base64.getEncoder().encodeToString(prefixAndSaltAndEncryptedData);
            System.out.println("Encode: " + encode);
            System.out.println("Decode: " + getJsonFromEncrypt(encode));
//            return Base64.getEncoder().encodeToString(prefixAndSaltAndEncryptedData);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }*/


    /*public static void main(String[] args) throws Exception {
        System.out.println(getJsonFromEncrypt("U2FsdGVkX1+KXqN70C25K9pm2e452VUr9vbeFxx9uzWaopKyvhpDum4ILeRpwmBg"));
    }*/
    //Post API
    public static final String BUY_CHEST_API = "/transaction/buy-chest";
    public static final String OPEN_CHEST_API = "/transaction/open-chest";
    public static final String TRANSFER_API = "/transaction/transfer";
    public static final String SELLING_API = "/transaction/selling";
    public static final String CANCEL_SELL_API = "/transaction/cancel-sell";
    public static final String AUCTION_API = "/transaction/auction";
    public static final String TRANSACTION_WITHDRAW_POINT_API = "/transaction/reduce";
    public static final String TRANSACTION_WITHDRAW_AMB_API = "/transaction/reduce-amb";
    public static final String TRANSACTION_DEPOSIT_POINT_API = "/transaction/buy-point";
    public static final String HERO_CHECK_FUSION_API = "/hero/check-fusion";
    public static final String UPGRADE_HERO_API = "/hero/upgrade";
    public static final String LOCK_HERO_API = "/hero/lock";
    public static final String UNLOCK_HERO_API = "/hero/unlock";

    public static final String LINK_EQUIPMENT_API = "/transaction/link-equipment";
    public static final String BUY_STAMINA_API = "/transaction/buy-stamina";

    //Get API
    public static final String LOGIN_API = "/user/login";
    public static final String BALANCE_API = "/user/balance";
    public static final String MARKET_API = "/market/nft";
    public static final String MARKET_NFT_INFORMATION_API = "/token";
    public static final String LIST_NFT_INFORMATION_API = "/token-list";
    public static final String INVENTORY_NFT_INFORMATION_API = "/inventory";
    public static final String HERO_IMAGE_API = "/heroes/image";
    public static final String HERO_REPORT_API = "/heroes/report";
    public static final String HERO_LIST_API = "/inventory/heroes";

    public static final String LIST_EQUIPMENT_NFT_INFORMATION_API = "/equipment/token-list";
    public static final String MARKET_EQUIPMENT_API = "/market/equipment";//
    public static final String EQUIPMENT_LIST_API = "/inventory/equipment";

    //Equipment API
    public static final String TRANSFER_EQUIPMENT_API = "/transaction/equipment/transfer";
    public static final String SELLING_EQUIPMENT_API = "/transaction/equipment/selling";
    public static final String CANCEL_EQUIPMENT_SELL_API = "/transaction/equipment/cancel-sell";
    public static final String EQUIPMENT_AUCTION_API = "/transaction/equipment/auction";
    public static final String EQUIPMENT_IMAGE_API = "/equipment/image";
    public static final String EQUIPMENT_NFT_INFORMATION_API = "/equipment/token";

    //Internal API
    public static final String GET_CONFIG_API = "/config";
    public static final String CHANGE_SHOP_CONFIG_API = "/config/shop";
    public static final String CHANGE_STAMINA_CONFIG_API = "/config/stamina";
    public static final String CHANGE_PVP_CONFIG_API = "/config/pvp";
    public static final String REFUND_ENO_API = "/config/refund";
    public static final String REFUND_STAMINA_API = "/config/refund/stamina";
    public static final String REFUND_ITEM_API = "/config/refund/item";
    public static final String COOP_REWARD_API = "/config/coop";

    //Report
    public static final String REPORT_SHOP_API = "/report/shop";
    public static final String REPORT_STAMINA_API = "/report/stamina";
    public static final String REPORT_ARENA_API = "/report/arena";
    public static final String REPORT_USER_API = "/report/user";


}
