package com.lta.okwar.market;

import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListEquipmentTokenInformationHandler extends GetRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
            super.handlerRequest(myExtension, req, resp, true);
//            sfsObject = new ObjectNode(JsonNodeFactory.instance);
//            List<String> headNames = Collections.list(req.getHeaderNames());
//            String token = req.getParameter("token_ids");
            String[] token = req.getParameterMap().get("token_ids");
            sfsObject.put("token_ids", String.join(",", token));
            resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.LIST_EQUIPMENT_TOKEN_IDS_INFORMATION_CMD, sfsObject);
    }
}
