package com.lta.okwar.market;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.PostRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class MarketHandler extends GetRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        super.handlerRequest(myExtension, req, resp, true);
        checkParams(req, "page");
        checkParams(req, "page_size");
        checkParams(req, "isEgg");
        checkParams(req, "is_on_auction");
        checkParamsMap(req, "gen");
        checkParamsMap(req, "rarity");
        checkParamsMap(req, "race");
        checkParamsMap(req, "element");
        checkParams(req, "level_min");
        checkParams(req, "level_max");
        checkParams(req, "sort");
        resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.GET_TOKEN_INFORMATION_LIST_CMD, sfsObject);
    }
}
