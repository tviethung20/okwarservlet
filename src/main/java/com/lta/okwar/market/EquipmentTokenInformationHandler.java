package com.lta.okwar.market;

import com.lta.okwar.GetRequestHandler;
import com.lta.okwar.SmartFoxCommand;
import com.smartfoxserver.v2.extensions.ISFSExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EquipmentTokenInformationHandler extends GetRequestHandler {
    public void handlerRequest(ISFSExtension myExtension, HttpServletRequest req, HttpServletResponse resp) throws IOException {
            super.handlerRequest(myExtension, req, resp, true);
//            sfsObject = new ObjectNode(JsonNodeFactory.instance);
//            List<String> headNames = Collections.list(req.getHeaderNames());
            String token = req.getParameter("token_id");
            sfsObject.put("token_id", token);
            resp.getWriter().write(SendRequest().toString());
    }

    @Override
    public Object SendRequest() {
        return myExtension.handleInternalMessage(SmartFoxCommand.GET_EQUIPMENT_TOKEN_DETAIL_CMD, sfsObject);
    }
}
